import { Dimensions } from "react-native";


export const data = {
    labels: ["Lun", "Mar", "Mer", "Jeu", "Ven", "Sa", "Dim" ],
    datasets: [
      {
        data: [3000, 2500, 1550, 1000, 800, 0, 1000],
        color: (opacity = 1) => `#FFDD0E`,
      }
    ]
  };


export const screenWidth = Dimensions.get("window").width;


export const chartConfig = {
    backgroundGradientFrom: "#1E2923",
    backgroundGradientFromOpacity: 0.5,
    backgroundGradientTo: "#08130D",
    backgroundGradientToOpacity: 1,
    color: (opacity = 1) => `#fff`,
    };

export const data2 = [
    {
      name: "€ Activités",
      population: 1000,
      color: "#30a123",
      legendFontColor: "#FFDD0E",
      legendFontSize: 13
    },
    {
      name: "€ Inutile",
      population: 500,
      color: "#076b22",
      legendFontColor: "#FFDD0E",
      legendFontSize: 13
    }, 
    {
      name: "€ Inutile",
      population: 700,
      color: "#28bf50",
      legendFontColor: "#FFDD0E",
      legendFontSize: 13
    }
  ]