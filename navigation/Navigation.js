import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';

import Landing from '../screens/Landing';
import Login from '../screens/Login';
import Register from '../screens/Register'
import HomeScreen from '../screens/HomeScreen'
import spendingMonth from '../screens/spendingMonth';
import spendingDay from '../screens/spendingDay';
import addExpense from '../screens/addExpense';

const Stack = createNativeStackNavigator();


export default function NativeStack(){ 
    return (
<NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Login" component={Login}
        options={{
          title: "Connexion",
          headerStyle: {
            backgroundColor: "#1B8366",
          },
          headerShadowVisible: false,
          headerTintColor: "#fff",
          headerTitleAlign: 'center',
        }}/>

        <Stack.Screen name="Register" component={Register}
        options={{
          title: "Inscription",
          headerStyle: {
            backgroundColor: "#1B8366",
          },
          headerShadowVisible: false,
          headerTintColor: "#fff",
          headerTitleAlign: 'center',
        }}/>

        <Stack.Screen name="ExpensesDay" component={spendingDay} 
        options={{
          title: "Dépenses des 30 derniers jours",
          headerStyle:{
            backgroundColor: '#1B8366',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerShadowVisible: false,
        }}/>
  
        <Stack.Screen name="ExpensesMonthly" component={spendingMonth} 
        options={{
          title: "Dépenses mois par mois",
          headerStyle:{
            backgroundColor: '#1B8366',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerShadowVisible: false,
        }}/>
        <Stack.Screen name="AddExpense" component={addExpense} 
        options={{
          title: "Ajouter une dépense",
          headerStyle:{
            backgroundColor: '#1B8366',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerShadowVisible: false,
        }}/>
        
      </Stack.Navigator>
  </NavigationContainer>
    )
}
