import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { FontAwesome5, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons'; 
import Navigation from './Navigation'


import HomeScreen from '../screens/HomeScreen'
import spendingMonth from '../screens/spendingMonth';
import spendingDay from '../screens/spendingDay';
import addExpense from '../screens/addExpense';
import expenseCategory from '../screens/expenseCategory';





const ExpensesHome = createNativeStackNavigator();

function HomeScreenNavigation() {
    return (
        <ExpensesHome.Navigator>
            <ExpensesHome.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    title: "Accueil",
                    headerStyle: {
                      backgroundColor: "#1B8366",
                    },
                    headerShadowVisible: false,
                    headerTintColor: "#fff",
                    headerTitleAlign: 'center',
                  }}
            />
        </ExpensesHome.Navigator>
    );
};


const SpendingDay = createNativeStackNavigator();

function SpendingDayNavigation() {
    return (
        <SpendingDay.Navigator>
            <SpendingDay.Screen name="expensesDay" component={spendingDay} 
            options={{ 
                    title: "Dépenses des 30 derniers jours",
                    headerStyle: {
                      backgroundColor: "#1B8366",
                    },
                    headerShadowVisible: false,
                    headerTintColor: "#fff",
                    headerTitleAlign: 'center',
                  }}/>
        </SpendingDay.Navigator>
    );
}

const SpendingMonth = createNativeStackNavigator();

function SpendingMonthNavigation() {
    return (
        <SpendingMonth.Navigator>
            <SpendingMonth.Screen name="expensesMonthly" component={spendingMonth} 
            options={{
                title: "Dépenses mois par mois",
                headerStyle: {
                  backgroundColor: "#1B8366",
                },
                headerShadowVisible: false,
                headerTintColor: "#fff",
                headerTitleAlign: 'center',
              }}/>
        </SpendingMonth.Navigator>
    );
}

const AddExpense = createNativeStackNavigator();

function AddExpenseNavigation() {
    return (
        <AddExpense.Navigator>
            <AddExpense.Screen name="addExpense" component={addExpense} 
            options={{
                title: "Ajouter une dépense",
                headerStyle: {
                  backgroundColor: "#1B8366",
                },
                headerShadowVisible: false,
                headerTintColor: "#fff",
                headerTitleAlign: 'center',
              }}/>
        </AddExpense.Navigator>
    );
}

const Categories = createNativeStackNavigator();

function CategoriesNavigation() {
    return (
        <Categories.Navigator>
            <Categories.Screen name="ExpensesCategory" component={expenseCategory} 
            options={{
                title: "Catégories des dépenses",
                headerStyle: {
                  backgroundColor: "#1B8366",
                },
                headerShadowVisible: false,
                headerTintColor: "#fff",
                headerTitleAlign: 'center',
              }}/>
        </Categories.Navigator>
    );
}

const Tab = createBottomTabNavigator();

export default function TabNav(){
    return (
        <NavigationContainer>
            <Tab.Navigator screenOptions={{ headerShown: false}}> 
                <Tab.Screen name="Accueil" options={{
                    tabBarIcon: ({ }) => (
                        <FontAwesome5 name="home" size={24} color="black" />
                    ),
                }}
                component={HomeScreenNavigation}/>
                <Tab.Screen name="30derniersjours" options={{
                    tabBarIcon: ({ }) => (
                        <MaterialIcons name="view-day" size={24} color="black" />
                    )
                }}
                component={SpendingDayNavigation}/>
                <Tab.Screen name="Mois" options={{
                    tabBarIcon: ({ }) => (
                        <MaterialCommunityIcons name="calendar-month" size={24} color="black" />
                    ),
                }}component={SpendingMonthNavigation}/>
                <Tab.Screen name="Ajout dépense"options={{
                    tabBarIcon: ({ }) => (
                        <FontAwesome5 name="money-check" size={24} color="black" />
                    ),
                }} component={AddExpenseNavigation}/>
                <Tab.Screen name="Catégories"options={{
                    tabBarIcon: ({ }) => (
                        <MaterialIcons name="category" size={24} color="black" />
                    ),
                }} component={CategoriesNavigation}/>
            </Tab.Navigator>
        </NavigationContainer>

    )
}
