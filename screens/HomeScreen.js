import React, { useEffect, useState } from 'react';
import { View, TouchableOpacity, Text } from "react-native";
import { homescreen } from '../Style/homescreen'
import { BarChart, PieChart } from "react-native-chart-kit";
import { btnstyle } from '../Style/btnstyle';
import { data, data2, chartConfig, screenWidth} from '../components/Chart';




  

export default function HomeScreen ({ navigation }){
    

    return (
    
        <View style={homescreen.container}>
            <View style={homescreen.view1}>
                <Text style={homescreen.welcome}>Bonjour, Tchoupi</Text>
                <Text style={homescreen.wallet}>Votre Porte-monnaie: 3000€</Text>
            </View>

            <View style={homescreen.view2}>
                <Text style={homescreen.subtitle}>Dépenses de cette semaine</Text>
                {/* Rajouter la balise du graphique */}

                <View style={homescreen.chart1}>

                <BarChart
                    data={data}
                    width={screenWidth}
                    height={220}
                    yAxisLabel="€"
                    chartConfig={chartConfig}
                    />
                </View>
            </View>

            <View style={homescreen.view3}>
                <Text style={homescreen.subtitle}>Dépenses du dernier mois</Text>
                {/* Rajouter les balise du graphique */}

                <View style={homescreen.chart2}>
                        <PieChart
                            data={data2}
                            width={screenWidth}
                            height={200}
                            chartConfig={chartConfig}
                            accessor={"population"}
                            absolute
                            />
                </View>
            </View>

        </View>
    )


}
