import React from "react";
import { useEffect, useState } from "react";
import { Text, View, FlatList } from "react-native";
import axios from "axios";

export default function ListDepDay() {

  const [expenses, setExpenses] = useState([]);
  useEffect(() => {
    async function getExpenses() {
      const res = await axios.get("http://127.0.0.1:8001/api/expenses");
      setExpenses(res.data["hydra:member"]);
    }
    getExpenses();
  }, []);
    return (
      <View>
        <FlatList
          data={expenses}
          renderItem={({item}) =>
          <View>
            <Text>{item.title}</Text>
            <Text>{item.value} Euros</Text>
            <Text>{item.category.title}</Text>
            <Text>{item.user.email}</Text>
            <Text>{item.category.id}</Text>
            <Text>{item.date}</Text>
          </View>}
        />
      </View>
    );

}