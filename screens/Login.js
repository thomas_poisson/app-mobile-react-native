import React from 'react';
import { View, StyleSheet, TextInput, TouchableOpacity, Text } from "react-native";
import { inputlogin } from '../Style/input';
import { stylelogin } from '../Style/input';
import { btnstyle } from '../Style/btnstyle';


export default function Login({ navigation }) {
    const [id, onChangeId] = React.useState("")
    const [password, onChangePassword] = React.useState("")


    return (
        <View style={stylelogin.container}>
            <TextInput
                style={inputlogin.inputid}
                onChangeText={onChangeId}
                value={id}
                placeholder="Identifiant/Adresse Mail">
            </TextInput>
            <TextInput
            style={inputlogin.inputpassword}
                onChangeText={onChangePassword}
                value={password}
                placeholder="Mot de passe">
            </TextInput>
            <TouchableOpacity
            onPress={() => navigation.navigate('Accueil')}
            style={btnstyle.loginbtn}>
            <Text style={btnstyle.connexion}>Connexion</Text>
            </TouchableOpacity>
        </View>
    )
    
}


