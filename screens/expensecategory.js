import React, { useEffect, useState } from "react";
import { View, SafeAreaView, TouchableOpacity, Text } from "react-native";
import { day } from "../Style/day";
import ListCat from "./ListCat";

export default function spendingDay() {
  return (
    <SafeAreaView>
      <View style={day.container}>
        <View style={day.view1}></View>
        <View>
          <ListCat />
        </View>
        
      </View>
    </SafeAreaView>
  );
}
