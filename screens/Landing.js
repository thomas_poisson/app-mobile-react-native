import React from 'react';
import { Text, View, TouchableOpacity} from 'react-native';
import { style } from '../Style/style';
import { btnstyle } from '../Style/btnstyle';


function Landing({ navigation }) {
    return (
      <View style={style.container}>
        <Text style={style.titre}>Cash</Text>
        <TouchableOpacity
          onPress={() => navigation.navigate('Register')}
          style={btnstyle.btninscription}>
          <Text style={btnstyle.inscription}>Inscription</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('Login')}
          style={btnstyle.btnconnexion}>
          <Text style={btnstyle.connexion}>Connexion</Text>
        </TouchableOpacity>
      </View>
    );
  }
  export default Landing;