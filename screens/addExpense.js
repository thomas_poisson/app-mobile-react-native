import React from 'react';
import { View, SafeAreaView, TouchableOpacity, TextInput, Text } from "react-native";
import {Picker} from '@react-native-picker/picker';
import { add } from '../Style/addExpense';

export default function addExpense (){
    const [spent, onChangeSpent] = React.useState("")
    const [categories, setCategories] = React.useState("")
    const [day, setDay] = React.useState("")

    return (
        <SafeAreaView>
            <View style={add.container}>
                <View style={add.view1}> 
                </View>
                <View style={add.view2}>
                <TextInput
                    style={add.inputSpent}
                    onChangeText={onChangeSpent}
                    keyboardType = 'numeric'
                    maxLength={5}
                    value={spent}
                    placeholder="Entrez la somme dépensée">
                </TextInput>
                <Picker
                selectedValue={categories}
                style={add.picker}
                onValueChange={(itemValue) => setCategories(itemValue)}
                >
                    <Picker.Item label="Activités" value="activites" />
                    <Picker.Item label="Divertissement" value="divertissement" />
                    <Picker.Item label="Nourriture" value="nourriture" />
                    <Picker.Item label="Bourse" value="bourses" />
                    <Picker.Item label="Charges" value="charges" />
                    <Picker.Item label="Famille" value="famille" />
                    <Picker.Item label="Sport" value="sport" />
                </Picker>
                <Picker
                selectedValue={day}
                style={add.picker}
                onValueChange={(itemValue) => setDay(itemValue)}
                >
                    <Picker.Item label="Lundi" value="Lundi" />
                    <Picker.Item label="Mardi" value="Mardi" />
                    <Picker.Item label="Mercredi" value="Mercredi" />
                    <Picker.Item label="Jeudi" value="Jeudi" />
                    <Picker.Item label="Vendredi" value="Vendredi" />
                    <Picker.Item label="Samedi" value="Samedi" />
                    <Picker.Item label="Dimanche" value="Dimanche" />
                 </Picker>
                </View>
            </View>
        </SafeAreaView>
        )


}