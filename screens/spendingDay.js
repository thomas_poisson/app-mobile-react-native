import React, { useEffect, useState } from "react";
import { View, SafeAreaView, TouchableOpacity, Text } from "react-native";
import { day } from "../Style/day";
import ListDepDay from "./ListDepDay";

export default function spendingDay() {
  return (
    <SafeAreaView>
      <View style={day.container}>
        <View style={day.view1}></View>
        <View>
          <ListDepDay />
        </View>
        
      </View>
    </SafeAreaView>
  );
}
