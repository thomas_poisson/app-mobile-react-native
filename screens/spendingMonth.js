import React from 'react';
import { useEffect, useState } from "react";
import { View, FlatList, Text } from "react-native";
import { month } from '../Style/month';
import axios from "axios";
import { ScreenContainer } from 'react-native-screens';



export default function spendingMonth(){

    const [expenses, setExpenses] = useState([]);
    useEffect(() => {
      async function getExpenses() {
        const res = await axios.get("http://127.0.0.1:8001/api/expenses");
        setExpenses(res.data["hydra:member"]);
      }
      getExpenses();
    }, []);
      return (
        <View style={month.container}>
          <FlatList
            data={expenses}
            renderItem={({item}) =>
            <View style={month.container}>
              <Text>{item.title}</Text>
              <Text>{item.value} Euros</Text>
              <Text>{item.category.title}</Text>
              <Text>{item.user.email}</Text>
              <Text>{item.date}</Text>
            </View>}
          />
        </View>
      );
}