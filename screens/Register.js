import React from 'react';
import { View, TextInput, TouchableOpacity, Text } from "react-native";
import { inputregister } from '../Style/input';
import { stylelogin } from '../Style/input';
import { btnstyle } from '../Style/btnstyle';

export default function Register({ navigation }) {
    const [name, onChangeName] = React.useState("")
    const [surname, onChangeSurname] = React.useState("")
    const [email, onChangeEmail] = React.useState("")
    const [password, onChangePassword] = React.useState("")
    const [reppassword, onChangeReppassword] = React.useState("")


    return (
        <View style={stylelogin.container}>
            <View style={stylelogin.view1}>
                <TextInput
                    style={inputregister.inputname}
                    onChangeText={onChangeName}
                    value={name}
                    placeholder="Nom">
                </TextInput>
                <TextInput
                style={inputregister.inputsurname}
                    onChangeText={onChangeSurname}
                    value={surname}
                    placeholder="Prénom">
                </TextInput>
                <TextInput
                    style={inputregister.inputmail}
                    onChangeText={onChangeEmail}
                    value={email}
                    placeholder="Adresse mail">
                </TextInput>
            </View>
            <View style={stylelogin.view2}>
                <TextInput
                style={inputregister.inputpassword}
                    onChangeText={onChangePassword}
                    value={password}
                    placeholder="Mot de passe">
                </TextInput>
                <TextInput
                    style={inputregister.inputreppassword}
                    onChangeText={onChangeReppassword}
                    value={reppassword}
                    placeholder="Répéter mot de passe">
                </TextInput>
                <TouchableOpacity
                onPress={() => navigation.navigate('Accueil')}
                style={btnstyle.registerbtn}>
                <Text style={btnstyle.inscription}>S'inscrire</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
    
}


