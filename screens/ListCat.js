import React from "react";
import { useEffect, useState } from "react";
import { Text, View, FlatList } from "react-native";
import axios from "axios";

export default function ListDepDay() {

  const [expenses, setExpenses] = useState([]);
  useEffect(() => {
    async function getExpenses() {
      const res = await axios.get("http://127.0.0.1:8001/api/categories");
      setExpenses(res.data["hydra:member"]);

    }
    getExpenses();
  }, []);
    return (
      <View>
        <FlatList
          data={expenses}
          renderItem={({item}) =>
          <View>
            <Text>{item.title}</Text>
          </View>}
        />
      </View>
    );

}