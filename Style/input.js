import { StyleSheet } from 'react-native';

export const stylelogin = StyleSheet.create({
    container:{
        backgroundColor: '#1B8366',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    view1:{
        marginBottom:20,
    },
    view2:{
        marginTop:20,
    }
})

export const inputlogin = StyleSheet.create({

    inputid:{
        backgroundColor: '#fff', 
        borderRadius: 8, 
        width:260, 
        height: 35,
        margin:8, 
        textAlign:'center',
        
    },
    inputpassword:{
        backgroundColor: '#fff', 
        borderRadius: 50, 
        borderRadius: 8, 
        width:260, 
        height: 35,
        margin:8, 
        textAlign: 'center',
    }

})

export const inputregister = StyleSheet.create({
    inputname:{
        backgroundColor: '#fff', 
        borderRadius: 50, 
        width:200, 
        height: 35,
        margin:8, 
        textAlign:'center'
    },
    inputsurname:{
        backgroundColor: '#fff', 
        borderRadius: 50, 
        width:200, 
        height: 35,
        margin:8, 
        textAlign:'center'
    },
    inputmail:{
        backgroundColor: '#fff', 
        borderRadius: 50, 
        width:200, 
        height: 35,
        margin:8, 
        textAlign:'center'
    },
    inputpassword:{
        backgroundColor: '#fff', 
        borderRadius: 50, 
        width:200, 
        height: 35,
        margin:8, 
        textAlign:'center'
    },
    inputreppassword:{
        backgroundColor: '#fff', 
        borderRadius: 50, 
        width:200, 
        height: 35,
        margin:8, 
        textAlign:'center'
    }

})