import { StyleSheet } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

export const style = StyleSheet.create({
    container: {
            flex: 1,
            backgroundColor: '#1B8366',
            alignItems: 'center',
            justifyContent: 'center',
      },
      titre:{
        fontWeight:'bold',
        fontSize:35,
        bottom:120,
        color: '#FFFFFF',
      },
      
    });


