import { StyleSheet } from 'react-native';

export const btnstyle = StyleSheet.create({

///Landing.js
    btninscription:{
        backgroundColor: '#FFDD0E', 
        borderRadius: 8, 
        width:170, 
        margin:16,
        padding:8,
    },
    btnconnexion:{
      backgroundColor: '#FFDD0E', 
      borderRadius: 8, 
      width:170, 
      padding:8,
    },
///

//Login.js
    loginbtn:{
      backgroundColor: '#FFDD0E', 
      borderRadius: 8, 
      width:130, 
      margin:18,
      padding:8,
    },
//Register.js
    registerbtn:{
      backgroundColor: '#FFDD0E', 
      borderRadius: 8, 
      width:130, 
      margin:40,
      padding:8,
    },


///Landing.js
    inscription:{
        color: '#000', 
        textAlign: 'center', 
  
      },
      connexion:{
        color: '#000', 
        textAlign: 'center', 
      },
///

///addExpense.js
    add:{
      backgroundColor: '#FFDD0E', 
      borderRadius: 8, 
      width:170, 
      marginLeft: 100,
      /* height:40 */
      padding:7,
      marginTop: 15,
    }
///
});