import { StyleSheet } from 'react-native';

export const day = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor: '#1B8366',
    },
    view1:{
        textAlign: 'center',
        height: 100,
        alignItems: 'center',
        justifyContent: 'center',
    },  

    welcome:{
        fontSize: 30,
        fontWeight:'bold',
        color:'#FFDD0E',
    },
    subtitle:{
        fontSize: 18,
        alignItems: 'flex-start',
        fontWeight:'bold',
        textAlign: 'center',
        color: '#fff',
    }

})