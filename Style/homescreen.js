import { StyleSheet } from 'react-native';

export const homescreen = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#1B8366',
    },
    view1:{
        textAlign: 'center',
        height: 100,
        alignItems: 'center',
        justifyContent: 'center',
    }, 
     wallet: {
        textAlign: 'center',
        fontWeight:'bold',
        fontSize:15,
        margin: 10,
        color: '#FFFFFF',
     },
    welcome:{
        fontSize: 30,
        fontWeight:'bold',
        color:'#FFDD0E',
    },
    subtitle:{
        fontSize: 18,
        alignItems: 'flex-start',
        fontWeight:'bold',
        textAlign: 'center',
        color: '#fff',
    },
    chart1:{
        backgroundColor: '#1B8366',
        height:200,
        marginTop: 10,
        marginBottom: 20,
    }, 
    chart2:{
        backgroundColor: '#1B8366',
        height:200,
    },
    add:{
        fontSize: 15,
        alignItems: 'flex-start',
        fontWeight:'bold',
        textAlign: 'center',
        color: '#fff',
        textAlignVertical:'auto'
    }


});