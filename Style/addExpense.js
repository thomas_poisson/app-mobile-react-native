import { StyleSheet } from 'react-native';


export const add = StyleSheet.create({
    container:{
        height: '100%',
        backgroundColor: '#1B8366',
    },
    view1:{
        textAlign: 'center',
        height: 100,
        alignItems: 'center',
        justifyContent: 'center',
    },  
    view2:{
        alignItems: 'center',
    },
    welcome:{
        fontSize: 30,
        fontWeight:'bold',
        color:'#FFDD0E',
    },
    subtitle:{
        fontSize: 18,
        alignItems: 'flex-start',
        fontWeight:'bold',
        textAlign: 'center',
        color: '#fff',
    },
    picker:{
        width:220,
        alignItems: "center",
        margin:8, 
    },
    inputSpent:{
        backgroundColor: '#fff', 
        borderRadius: 8, 
        width:220, 
        height: 35,
        margin:8, 
        textAlign:'center',
    },

})